/* Comandos de inserção*/----------------------------------------------------------------------------------------------
INSERT INTO nomedatabela(id, campo1, campo2, campo3)
VALUES (valor1, valor2, valor3);

/*Comandos de alteração*/----------------------------------------------------------------------------------------------

 UPDATE nomedatabela SET campoparaalterar = ValorParaAlterar WHERE campoqualquer = algumvalorespecífico;
 
 
 /*Comando de Deleção*/----------------------------------------------------------
 
DELETE FROM nomedatabela WHERE campoqualquer = algumvalorespecífico;

/*Comando de Consulta*/

--Opção 1: Buscando tudo na tabela usando o operador (*)

SELECT * FROM nomedatabela WHERE campo = algumvalorespecífico;

--Opção 2: Buscando campos específicos da tabela

SELECT campo1, campo2, campo3 FROM nomedatabela WHERE campo = algumvalorespecífico;