USE auroradb;

CREATE TABLE cadastro (
id_cadastro int NOT NULL AUTO_INCREMENT,
nome_pc varchar(30) NOT NULL,
descricao varchar(200),
id_categoria_produto int NOT NULL,
qtde int not null,
PRIMARY KEY (id_cadastro)
);

CREATE TABLE categoria (
id_categoria int NOT NULL auto_increment,
descricao varchar(30) NOT NULL,
PRIMARY KEY (id_categoria)
);

CREATE TABLE tier (
id_tier int NOT NULL auto_increment,
titulo varchar(15) NOT NULL,
Ram int NOT NULL,
ROM int NOT NULL,
CPU varchar(20) NOT NULL,
PRIMARY KEY(id_tier)
);



CREATE TABLE funcionario (
id_funcionario INT not null auto_increment,
nome int not null,
login varchar(25),
senha varchar(255),
PRIMARY KEY (id_funcionario)
);

