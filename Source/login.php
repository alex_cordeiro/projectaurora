<!DOCTYPE html>
<?php 
include 'connect.php';
?>

<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="./estilos/login.css">
</head>

<body>
    <div class="logincontainer">
        
        
        <div class="loginform ">
        <center>
            <img src="./imagens/login.png" alt="Login">
        
            <form class="form-group" method="POST" action="funcoes/processaLogin.php">
                <input class=" form-control" type="text" name="username" placeholder="Login"  required>
                <input class=" form-control" type="password" name="senha" placeholder="Password" autocomplete="off" required><br>
                <button class="form control btn bg-primary" type="submit" name="enviar">Enviar</button>
            </form>
        </center>
        </div>
    </div>

    <div class="copyright">
        Aurora informática &reg;
    </div>

    
</body>

</html>